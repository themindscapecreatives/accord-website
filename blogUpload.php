	<?php
	session_start();
    if($_SESSION['success'] === "active")
    { ?>
<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content=""/>
	<meta name="author" content=""/>
	<meta name="robots" content=""/>
	<meta name="description" content="Accord Construction Company - Coimbatore,TN"/>
	<meta property="og:title" content="Accord Construction Company"/>
	<meta property="og:description" content="Design Your Dream House With Us!"/>
	<meta property="og:image" content="https://www.accordbuildcorp.in/images/titleLogo1.png"/>
	<meta name="format-detection" content="telephone=no">

	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Title -->
	<title>Blog upload - Accord Construction Company</title>

	<!-- Favicon icon -->
    <link rel="icon" type="image/x-icon" href="images/titleLogo.png">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
	<!-- Custom Stylesheet -->
	<link href="vendor/lightgallery/css/lightgallery.min.css" rel="stylesheet">
	<link href="vendor/aos/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="vendor/rangeslider/rangeslider.css">
    
	<link rel="stylesheet" class="skin" href="css/skin/skin-1.css">

</head>
<body id="bg" class="theme-rounded">

		<div id="loading-area" class="loading-page-1">
			<div class="spinner">
				<div class="ball"></div>
				<p>LOADING</p>
			</div>
		</div>
		<div class="page-wraper">
			<!-- Header -->
			<header class="site-header mo-left header style-1">
				<!-- Header Top Bar -->
				<div class="top-bar">
					<div class="container-fluid">
						<div class="dz-topbar-inner d-flex justify-content-between align-items-center">
											</div>
					</div>
				</div>

				<!-- Main Header -->
				<div class="sticky-header main-bar-wraper navbar-expand-lg">
					<div class="main-bar clearfix ">
						<div class="container-fluid clearfix">
							<!-- Website Logo -->
							<div class="logo-header mostion logo-dark">
								<a href="index.html"><img src="images/footerLogo.png" alt=""></a>
							</div>

							<div class="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
								<ul class="nav navbar-nav navbar navbar-right">
								<li><a href="deleteGridBlog.php?message='Welcome to Admin Delete Panel'"><h4>Delete Blogs</h4></a></li>
									<li><a href="logout.php?logout"><h4>Logout</h4></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- Main Header End -->
			</header>
			<!-- Header End -->
			<div class="contact-sidebar">
				<div class="contact-box">
					<div class="logo-contact">
						<a href="index.html"><img src="images/logo.png" alt=""></a>
					</div>

				</div>
			</div>

			<div class="page-content bg-white">
				<!-- Banner  -->
				<div class="dz-bnr-inr style-1 overlay-white-dark" >
					<center><h1>Accord Admin Panel</h3> </center>
				</div>
				<section class="content-inner-1 pt-0">

					<div class="container">
						<div class="contact-area1 aos-item" data-aos="fade-up" data-aos-duration="800" data-aos-delay="400">

							<form class="dz-form style-1 dzForm" >
								<input type="hidden" class="form-control" name="dzToDo" value="Contact">
								<div class="dzFormMsg"></div>
								<div class="row sp10">
									<div class="col-sm-12 m-b20">
										<div class="input-group">
											<input type="text" class="form-control" id="author_name" name="dzOther[author_name]" placeholder="Author Name">
										</div>
									</div>
									<div class="col-sm-6 m-b20">
										<div class="input-group">
											<input type="text" class="form-control" id="blog_title" name="dzOther[blog_title]" placeholder="Blog Title">
										</div>
									</div>
									<div class="col-sm-6 m-b20">
										<div class="input-group">
											<label for="fileupload">Choose Blog Picture
											<input type="file" name="image" id = "imageBlog"></label>
											<!-- <input type="text" class="form-control" id="form_contact" name="dzOther[img_url]" placeholder="Image URL"> -->
										</div>
									</div>
									<!-- <div class="col-sm-12 m-b20">
										<div class="input-group">
											<input type="text" class="form-control" id="form_subject" name="dzOther[subject]" placeholder="Subject">
										</div>
									</div> -->
									<div class="col-sm-12 m-b20">
										<div class="input-group">
											<textarea name="blogBody"  id="blogBody" class="form-control" placeholder="Blog Body"></textarea>
										</div>
									</div>
									<div class="col-sm-12 text-center">
										<button name="submit"  class="btn btn-primary btn-rounded" onclick = sendData()>Post Blog</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
				
			</div>
			<!-- Footer -->
			<footer class="site-footer style-1" id="footer" style="background-image:url(images/background/pattern3.png)">
				<div class="footer-top">
					<div class="container">
						<div class="row footer-logo-head spno" style="padding-bottom: 0px;">
							<div class="col-md-6 aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
								<div class="footer-logo">
									<img src="images/footerLogo.png" alt="" style="height:100px;">
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
				
				<!-- Footer Bottom -->
				<div class="footer-bottom">
					<div class="container">
						<div class="row align-items-center fb-inner spno">
							<div class="col-lg-12 col-md-12 text-center"> 
								<span class="copyright-text">Copyright © 2022 <a href="https://www.accordbuildcorp.in/" class="text-primary" target="_blank">ACCORD CONSTRUCTION COMPANY</a> All rights reserved.</span>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>	
		<!-- JAVASCRIPT FILES ========================================= -->
		<script>	


		function sendData()
			{
				const FD  = new FormData();
				var author_name = document.getElementById("author_name").value;
				var blog_title = document.getElementById("blog_title").value; 
				var image = document.getElementById("imageBlog");
				var blogBody = document.getElementById("blogBody").value;

				FD.append( "author_name", author_name );
				FD.append( "blog_title", blog_title );
				FD.append( "blogBody", blogBody );


				const uploadFile = async (FD , file) => {
					await FD.append('image', file);
					// send `POST` request
					await fetch('insertBlog.php', {
						method: 'POST',
						body: FD
					})
					.then(res => res.json())
					//.then(json => console.log(json))
					.then(alert("Blog Saved"))
					.catch(err => console.error(err));
				}

				uploadFile(FD , image.files[0]);
			}

		</script>


		<script src="js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- BOOTSTRAP.MIN JS -->
		<script src="vendor/switcher/switcher.js"></script><!-- SWITCHER -->
		<script src="vendor/rangeslider/rangeslider.js"></script><!-- RANGESLIDER -->
		<script src="vendor/lightgallery/js/lightgallery-all.min.js"></script><!-- LIGHTGALLERY -->
		<script src='https://www.google.com/recaptcha/api.js'></script> <!-- Google API For Recaptcha  -->
		<script src="vendor/aos/aos.js"></script><!-- AOS -->
		<script src="js/dz.ajax.js"></script><!-- AJAX -->
		<script src="js/custom.js"></script><!-- CUSTOM JS -->
		<script src="js/form.js"></script>

</body>
</html>
<?php
	}
	else{
		header("location:loginPage.php");
		exit();
	}