<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content=""/>
	<meta name="author" content=""/>
	<meta name="robots" content=""/>
	<meta name="description" content="Accord Construction Company - Coimbatore,TN"/>
	<meta property="og:title" content="Accord Construction Company"/>
	<meta property="og:description" content="Design Your Dream House With Us!"/>
	<meta property="og:image" content="https://www.accordbuildcorp.in/images/titleLogo1.png"/>
	<meta name="format-detection" content="telephone=no">

	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Title -->
	<title>Blog details - Accord Construction Company</title>

	<!-- Favicon icon -->
    <link rel="icon" type="image/png" href=ages/favicon.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
	<!-- Custom Stylesheet -->
	<link href="vendor/lightgallery/css/lightgallery.min.css" rel="stylesheet">
	<link href="vendor/aos/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="vendor/rangeslider/rangeslider.css">
	<link rel="stylesheet" class="skin" href="css/skin/skin-1.css">


</head>
<body id="bg" class="theme-rounded">
<div id="loading-area" class="loading-page-1">
	<div class="spinner">
		<div class="ball"></div>
		<p>LOADING</p>
	</div>
</div>
<?php
$servername = "localhost";
$username = "u225439693_accordbuildcor";
$password = "admin@TMC_22";
$db = "u225439693_accord";

$conn = mysqli_connect ($servername , $username , $password , $db);

if($conn){
	$blog_id = $_GET['id']; ?>
<div class="page-wraper">
	<!-- Header -->
	<!-- Header -->
	<header class="site-header mo-left header style-1">
		<!-- Header Top Bar -->
		<div class="top-bar">
			<div class="container-fluid">
				<div class="dz-topbar-inner d-flex justify-content-between align-items-center">
					<div class="dz-topbar-left">
						<ul class="dz-social-icon">
							<li><a class="fab fa-facebook-f" href="https://www.facebook.com/accordbuildcorp/"></a></li>
							<li><a class="fab fa-instagram" href="https://www.instagram.com/accordbuildcorp/"></a></li>
							
						</ul>
					</div>
					<div class="dz-topbar-right">
						<ul>
							<li><i class="fas fa-map-marker-alt"></i>Coimbatore, TN</li>
							<li><i class="far fa-envelope"></i> accordbuildcorp@outlook.com</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Main Header -->
		<div class="sticky-header main-bar-wraper navbar-expand-lg">
			<div class="main-bar clearfix ">
				<div class="container-fluid clearfix">
					<!-- Website Logo -->
					<div class="logo-header mostion logo-dark">
						<a href="index.html"><img src="images/footerLogo.png" alt=""></a>
					</div>
					<!-- Nav Toggle Button -->
					<button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<!-- Extra Nav -->
					
					<div class="extra-nav">
						<div class="extra-cell">
							
							<div class="extra-icon-box" onclick="copy_clipboard()">
								<i class="fas fa-phone-alt"></i>
								<span>Call Now</span>
								<h4 class="title" id ="copy_clipboard">+91 807 2680 721</h4>
							</div>
						
						</div>

					</div>
					<!-- Extra Nav -->
					
					<div class="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
						<div class="logo-header">
							<a href="index.html"><img src="images/footerLogo.png" alt=""></a>
						</div>
						<ul class="nav navbar-nav navbar navbar-left">
							<li><a href="/">Home</a></li>	
							
							<li><a href="about-us.html">About Us</a></li>
							
							<li><a href="services.html">Services</a></li>
							
							<li><a href="gridBlog.php">Blogs</a></li>
							<li><a href="contact-us.html">Contact Us</a></li>
						</ul>
						<div class="dz-social-icon">
							<ul>
								<li><a class="fab fa-facebook-f" href="https://www.facebook.com/accordbuildcorp/"></a></li>
								
								<li><a class="fab fa-instagram" href="https://www.instagram.com/accordbuildcorp/"></a></li>
							</ul>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<!-- Main Header End -->
	</header>
	<!-- Header End -->

	<!-- InquiryModal -->
	<div class="modal fade inquiry-modal" id="exampleModal" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="inquiry-adv">
				<img src="images/img1.jpg" alt=""/>
			</div>
			<div class="modal-content">
				<div class="modal-header">
					<i class="flaticon-email"></i>
					<h5 class="modal-title" id="exampleModalLongTitle">Subscribe To Our Newsletter</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">&times;</button>
				</div>
				<div class="modal-body">
					<div class="dzFormMsg"></div>
					<form action="script/mailchamp.php" class="dzForm" method="post">
						<div class="form-group mb-3">
							<input type="text" name="dzName" required class="form-control" placeholder="YOUR NAME">
						</div>
						<div class="form-group mb-3">
							<input type="email" name="dzEmail" required class="form-control" placeholder="YOUR EMAIL ADDRESS">
						</div>
						<div class="form-group text-center">
							<button name="submit" type="submit" value="Submit" class="btn btn-primary">SUBSCRIBE NOW</button>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="page-content bg-white">
		<!-- Banner  -->
		<div class="dz-bnr-inr style-1 overlay-white-dark" style="background-image: url(images/banner/bnr8.jpg);">
			<div class="container">
				<div class="dz-bnr-inr-entry">
					<h1>Blog Detail</h1>
					<!-- Breadcrumb Row -->
					<nav aria-label="breadcrumb" class="breadcrumb-row">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="index.html">Home</a></li>
							<li class="breadcrumb-item">Blog</li>
							<li class="breadcrumb-item">Blog Detail</li>
						</ul>
					</nav>
					<!-- Breadcrumb Row End -->
				</div>
			</div>
			<div class="follow-info">
				<div class="inner-info">
					<h6 class="title text-primary">Follow Us On:</h6>
					<ul>
						<li><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-youtube"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-dribbble"></i></a></li>
						<li><a href="javascript:void(0);"><i class="fab fa-pinterest-p"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Banner End -->

		<!-- Blog Large -->
		<div class="content-inner bg-img-fix">
			<div class="container">
				<div class="row">
					<div class="col-xl-4 col-lg-4 m-b30 dz-order-1">
						<aside class="side-bar sticky-top left">
							<div class="widget widget_tag_cloud">
                                
                            </div>
                           <div class="widget widget_categories">
                                <div class="widget-title">
									<h5 class="title">Categories</h5>
									<div class="dz-separator style-1 text-primary mb-0"></div>
								</div>
                                <ul>
                                    <li class="cat-item"><a href="gridBlog.php">default</a></li>
                                </ul>
                            </div>
                            <div class="widget recent-posts-entry">
                                <div class="widget-title">
									<h5 class="title">Recent Posts</h5>
									<div class="dz-separator style-1 text-primary mb-0"></div>
								</div>
                               <div class="widget-post-bx">
								   <?php
								   		$sql = "SELECT id, created, blog_author, blog_title, blog_content, image FROM blogs ORDER BY created DESC LIMIT 5";
										$result = $conn->query($sql);

										if ($result->num_rows > 0){
											$row = $result->fetch_assoc();
											$i = 0;

											while($row = $result->fetch_assoc()) { ?>
												<div class="widget-post clearfix">
													<div class="dz-media">
														<a href="blog-details.php?id=<?=$row['id']?>"><img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['image']); ?>" alt=""></a>
													</div>
													<div class="dz-info">
														<h4 class="title"><a href="blog-details.php?id=<?=$row['id']?>"><?php echo $row["blog_title"]; ?></a></h4>
														<div class="dz-meta">
															<ul>
																<li class="post-date"> <?php echo date('F jS, Y',strtotime($row["created"])); ?> </li>
															</ul>
														</div>
													</div>
												</div>
												<?php $i = $i + 1;
												if($i > 3){break;}
											}
										} ?>

                                </div>
                            </div>
							
                        </aside>
					</div>


					<?php
							$sql = "SELECT id, created, blog_author, blog_title, blog_content, image FROM blogs where id = $blog_id";
							$result = $conn->query($sql);

							if ($result->num_rows > 0){
								$row = $result->fetch_assoc()
								?>

					<div class="col-xl-8 col-lg-8 m-b20">
						<div class="dz-card blog-single style-1">
							<div class="dz-info">
								<div class="dz-meta">
									<ul>
										<li class="post-date"><?php echo date('F jS, Y',strtotime($row["created"])); ?></li>
										<li class="post-user">By <a href="javascript:void(0);"><?php echo $row["blog_author"]; ?></a></li>
									</ul>
								</div>
								<h2 class="dz-title"><?php echo $row["blog_title"];?> </h2>
							</div>
							<div class="dz-media">
								<img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['image']);?>" alt="">
							</div>
							<div class="dz-info">
								<div class="dz-post-text">
								<?php echo base64_decode($row["blog_content"]); }?>
								</div>
								<div class="dz-share-post">
									<h5 class="title">Visit us :</h5>
									<ul class="dz-social-icon">
										<li><a href="https://www.facebook.com/accordbuildcorp/" class="fab fa-facebook-f"></a></li>
										<li><a href="https://www.instagram.com/accordbuildcorp/" class="fab fa-instagram"></a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- blog start -->
						<div class="row extra-blog style-1">
							<div class="col-lg-12">
								<div class="widget-title">
									<h5 class="title">Related Blogs</h5>
									<div class="dz-separator style-1 text-primary mb-0"></div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="row">
								<?php
								   		$sqlr = "SELECT id, created, blog_author, blog_title, blog_content, image FROM blogs ORDER BY blog_title, blog_content DESC LIMIT 3";
										$resultr = $conn->query($sqlr);

										if ($resultr->num_rows > 0){
											$row = $resultr->fetch_assoc();
											$i = 0;

											while($row = $resultr->fetch_assoc()) { ?>
												<div class="col-md-6">
													<div class="dz-card blog-grid style-1 m-b30">
														<div class="dz-media">
															<a href="blog-details.php?id=<?=$row['id']?>"><img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['image']);?>" alt=""></a>
														</div>
														<div class="dz-info">
															<div class="dz-meta">
																<ul>
																	<li class="post-date"><?php echo date('F jS, Y',strtotime($row["created"])); ?></li>
																	<li class="post-user">By <a href="javascript:void(0);"><?php echo $row["blog_author"]; ?></a></li>
																</ul>
															</div>
															<h5 class="dz-title"><a href="blog-details.php?id=<?=$row['id']?>"><?php echo $row["blog_title"];?></a></h5>
															<div class="dz-post-text text text-truncate">
																<p><?php echo base64_decode($row["blog_content"]); ?> </p>
															</div>
															<a href="blog-details.php?id=<?=$row['id']?>" class="btn-link">Read More</a>
														</div>
													</div>
												</div>
												<?php $i = $i + 1;
												if($i > 1){break;}
											}
										}} ?>

								</div>
							</div>
						</div>


                       
					</div>
				</div>
			</div>
		</div>
		
	</div>
		
    <!-- Footer -->
    <footer class="site-footer style-1" id="footer" style="background-image:url(images/background/pattern3.png)">
		<div class="footer-top">
            <div class="container">
				<div class="row footer-logo-head spno" style="padding-bottom: 0px;">
					<div class="col-md-6 aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
						<div class="footer-logo">
							<img src="images/footerLogo.png" alt="" style="height:100px;">
						</div>
					</div>
					<div class="col-md-6 text-md-end aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
						<ul class="social-list style-1">
							<li><a href="https://www.facebook.com/accordbuildcorp/"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="https://www.instagram.com/accordbuildcorp/"><i class="fab fa-instagram"></i></a></li>
							
						</ul>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-12 col-lg-6 aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">
						<div class="widget widget_gallery me-0 me-lg-5">
							<ul class="lightgallery">
								<li>
									<div class="dlab-post-thum dlab-img-effect">
										<span>		
											<img src="images/services/pic1.jpg" alt=""> 
										</span>
									</div>
								</li>
								<li>
									<div class="dlab-post-thum dlab-img-effect">
										<span>		
											<img src="images/services/pic2.jpg" alt=""> 
										</span>
									</div>
								</li>
								<li>
									<div class="dlab-post-thum dlab-img-effect">
										<span>		
											<img src="images/services/pic3.jpg" alt=""> 
										</span>
									</div>
								</li>
								<li>
									<div class="dlab-post-thum dlab-img-effect">
										<span>		
											<img src="images/services/pic4.jpg" alt=""> 
										</span>
									</div>
								</li>
								<li>
									<div class="dlab-post-thum dlab-img-effect">
										<span>		
											<img src="images/services/pic5.jpg" alt=""> 
										</span>
									</div>
								</li>
								<li>
									<div class="dlab-post-thum dlab-img-effect">
										<span>		
											<img src="images/services/pic6.jpg" alt=""> 
										</span>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="800">
						<div class="widget widget_categories">
							<div class="widget-title">
								<h4 class="title">Our Links</h4>
								<div class="dz-separator style-1 text-primary mb-0"></div>
							</div>
							<ul class="list-2">
								<li class="cat-item"><a href="index.html">Home</a></li>                                         
								<li class="cat-item"><a href="about-us.html">About Us</a></li>                                         
							                                        -->
								<li class="cat-item"><a href="services.html">Services</a></li>                                         
								
								<li class="cat-item"><a href="blog-grid.html">Blogs</a></li> 
								
								<li class="cat-item"><a href="contact-us.html">Contact Us</a></li> 
							</ul>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1000">
						<div class="widget widget_getintuch">
							<div class="widget-title">
								<h4 class="title">Contact Us</h4>
								<div class="dz-separator style-1 text-primary mb-0"></div>
							</div>
							<ul>
								<li><i class="flaticon-telephone"></i><h5>Phone Number</h5> +91 807 2680 721</li>
								<li><i class="flaticon-email"></i><h5>Email Address</h5>accordbuildcorp@outlook.com</li>
								<li><i class="flaticon-maps-and-location"></i><h5>Office Address</h5>Coimbatore,TN</li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row align-items-center fb-inner spno">
					<div class="col-lg-12 col-md-12 text-center"> 
						<span class="copyright-text">Copyright © 2022 <a href="https://www.accordbuildcorp.in/" class="text-primary" target="_blank">ACCORD CONSTRUCTION COMPANY</a> All rights reserved.</span>
					</div>
					
                </div>
            </div>
        </div>
    </footer>
	<div class="col-md-6 text-md-end aos-item social-media-side" style="z-index: 5;" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
		<ul class="social-list style-1">
			<li><a href="https://www.facebook.com/accordbuildcorp/"><i class="fab fa-facebook-f"></i></a></li>
			<li><a href="https://www.instagram.com/accordbuildcorp/"><i class="fab fa-instagram"></i></a></li>
			
		</ul>
	</div>
    <!-- Footer End -->
	<button class="scroltop icon-up" type="button"><i class="fas fa-arrow-up"></i></button>
</div>	
<!-- JAVASCRIPT FILES ========================================= -->
<script src="js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- BOOTSTRAP.MIN JS -->

<script src="vendor/rangeslider/rangeslider.js"></script><!-- RANGESLIDER -->
<script src="vendor/lightgallery/js/lightgallery-all.min.js"></script><!-- LIGHTGALLERY -->
<script src="vendor/aos/aos.js"></script><!-- AOS -->
<script src="js/dz.ajax.js"></script><!-- AJAX -->
<script src="js/custom.js"></script><!-- CUSTOM JS -->
</body>
</html>