// fetch("https://api.apispreadsheets.com/data/16589/", {
// 	method: "POST",
// 	body: JSON.stringify({"data": {"name":"","email":"","subject":"","message":"","contact_no":"","time_stamp":""}, "query": "select*from16589wherename=''"}),
// }).then(res =>{
// 	if (res.status === 201){
// 		// SUCCESS
// 	}
// 	else{
// 		// ERROR
// 	}
// })

function form_submit(){
    var name = document.getElementById("form_name").value;
 
    var email = document.getElementById("form_email").value; 
  
    var message = document.getElementById("form_message").value;
    
    var subject = document.getElementById("form_subject").value;
    
    var contact = document.getElementById("form_contact").value;
    
    var timeStamp = new Date().toISOString();

    name && email && message &&
    fetch("https://api.apispreadsheets.com/data/16629/", {
        method: "POST",
        body: JSON.stringify({"data": {"name":name,"email":email,"message":message,"subject":subject,"contact_no": contact,"time_stamp":timeStamp}}),
        }).then(res =>{
            if (res.status === 201){
                // SUCCESS
                console.log("done");
                document.getElementById("form_name").value = "";
                document.getElementById("form_email").value = "";
                document.getElementById("form_message").value = "";
                document.getElementById("form_subject").value = "";
                document.getElementById("form_contact").value = "";
				//hotsnackbar( false,'message sent !');
            }
            else{
                // ERROR
                console.log("error sending message");
            }
        })
}

function popup_submit(){
    var name = document.getElementById("dzName").value;
    // document.getElementById("form_name").value = "";
    var email = document.getElementById("dzEmail").value;
    // document.getElementById("form_email").value = "";
    var contact = document.getElementById("dzContact").value;
    var category  = document.getElementById("dzCategory").value;
    // document.getElementById("form_contact").value = "";
    var timeStamp = new Date().toISOString();

    name && email && 
    fetch("https://api.apispreadsheets.com/data/19773/", {
        method: "POST",
        body: JSON.stringify({"data": {"name": name,"email": email,"contact_no": contact,"category": category,"time_stamp": timeStamp}}),
    }).then(res =>{
        if (res.status === 201){
            // SUCCESS
            document.getElementById("popup").modal("hide");
        }
        else{
            // ERROR
        }
    })
}
