<?php
	session_start();
    if($_SESSION['success'] === "active")
    { ?>

<html lang="en">
<head>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content=""/>
	<meta name="author" content=""/>
	<meta name="robots" content=""/>
	<meta name="description" content="Accord Construction Company - Coimbatore,TN"/>
	<meta property="og:title" content="Accord Construction Company"/>
	<meta property="og:description" content="Design Your Dream House With Us!"/>
	<meta property="og:image" content="https://www.accordbuildcorp.in/images/titleLogo1.png"/>
	<meta name="format-detection" content="telephone=no">

	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Title -->
	<title>Blog Delete - Accord Construction Company</title>

	<!-- Favicon icon -->
    <link rel="icon" type="image/png" href="images/favicon.png">

	<!-- Stylesheet -->
	<link href="vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
	<link href="vendor/aos/aos.css" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
	<!-- Custom Stylesheet -->
	<link href="vendor/lightgallery/css/lightgallery.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="vendor/rangeslider/rangeslider.css">
   
	<link rel="stylesheet" class="skin" href="css/skin/skin-1.css">
		
</head>
<body id="bg" class="theme-rounded">
<div id="loading-area" class="loading-page-1">
	<div class="spinner">
		<div class="ball"></div>
		<p>LOADING</p>
	</div>
</div>
<div class="page-wraper">
	<!-- Header -->
	<!-- Header -->
	<header class="site-header mo-left header style-1">
		<!-- Header Top Bar -->
		<div class="top-bar">
			<div class="container-fluid">
				<div class="dz-topbar-inner d-flex justify-content-between align-items-center">
					<div class="dz-topbar-left">
						<ul class="dz-social-icon">
							<li><a class="fab fa-facebook-f" href="https://www.facebook.com/accordbuildcorp/"></a></li>
							<li><a class="fab fa-instagram" href="https://www.instagram.com/accordbuildcorp/"></a></li>
						</ul>
					</div>
					<div class="dz-topbar-right">
						<ul>
							<li><i class="fas fa-map-marker-alt"></i>Coimbatore, TN</li>
							<li><i class="far fa-envelope"></i> accordbuildcorp@outlook.com</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Main Header -->
		<div class="sticky-header main-bar-wraper navbar-expand-lg">
			<div class="main-bar clearfix ">
				<div class="container-fluid clearfix">
					<!-- Website Logo -->
					<div class="logo-header mostion logo-dark">
						<a href="index.html"><img src="images/footerLogo.png" alt=""></a>
					</div>
					<!-- Nav Toggle Button -->
					<button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<!-- Extra Nav -->
					
					<div class="extra-nav">
						<div class="extra-cell">
							<div class="extra-icon-box" onclick="copy_clipboard()">
								
							</div>
						</div>

					</div>
					<div class="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
						<div class="logo-header">
							<a href="index.html"><img src="images/footerLogo.png" alt=""></a>
						</div>
						<div class="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
								<ul class="nav navbar-nav navbar navbar-right">
									<li><a href="logout.php?logout"><h4>Logout</h4></a></li>
								</ul>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<!-- Main Header End -->
	</header>
	<!-- Header End -->


	<div class="page-content bg-white">
		<!-- Banner  -->
		<div class="dz-bnr-inr style-1 overlay-white-dark" style="background-image: url(images/banner/bnr3.jpg);">
			<div class="container">
				<div class="dz-bnr-inr-entry">
					<h1>Our Blog</h1>
					<!-- Breadcrumb Row -->
					<nav aria-label="breadcrumb" class="breadcrumb-row">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="/">Home</a></li>
							<li class="breadcrumb-item">Our Blog</li>
						</ul>
					</nav>
					<!-- Breadcrumb Row End -->
				</div>
			</div>
		</div>
		<!-- Banner End -->
		<!-- Blog Large -->

    <?php
    $servername = "localhost";
	$username = "u225439693_accordbuildcor";
	$password = "admin@TMC_22";
	$db = "u225439693_accord";

	$conn = mysqli_connect ($servername , $username , $password , $db);

			if($conn){
				$message = $_GET['message'];
				if(!($message === "")){
					echo("<script>alert(".$message.");</script>");
				}

				$sql = "SELECT id, created, blog_author, blog_title, blog_content, image FROM blogs";
				$result = $conn->query($sql);

				if ($result->num_rows > 0){  ?>

					<div class="content-inner">
						<div class="container">
						<div class="row" id="masonry">
							<?php  while($row = $result->fetch_assoc()) {  ?>

							<div class="col-xl-6 col-lg-6 card-container">
								<div class="dz-card blog-grid style-1 m-b50 aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
								<div class="dz-media">
									<img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['image']); ?>" alt="">
								</div>
								<div class="dz-info">
									<div class="dz-meta">
										<ul>
											<li class="post-date"><?php echo date('F jS, Y',strtotime($row["created"])); ?></li>
											<li class="post-user">By <a href="javascript:void(0);"><?php echo $row["blog_author"]; ?></a></li>
										</ul>
									</div>
								<h3 class="dz-title" id = "titleText"><?php echo $row["blog_title"]; ?> </h3>
								<div class="dz-post-text text text-truncate">
									<p id = "paraText"><?php echo base64_decode($row["blog_content"]); ?> </p>
								</div>
								<a href="blog-delete.php?id=<?=$row['id']?>" class="btn-link">Delete this blog</a>
							</div>
						</div>

					</div>
    <?php
    }
    }
				else { ?>
					<div class="content-inner">
					<div class="container">
					<div class="row" id="masonry">
					<div class="col-xl-6 col-lg-6 card-container">

					<div class="dz-card blog-grid style-1 m-b50 aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
						<div class="dz-media">
							<a href="index.html"><img src="images\blog\blog-grid\pic2.jpg" alt="blog image comes here"></a>
						</div>
						<div class="dz-info">
							<div class="dz-meta">

							</div>
							<h3 class="dz-title"><a href="index.html" id = "titleText">No Blogs</a></h3>
							<div class="dz-post-text text">
								<p id = "paraText">Nothing to display</p>
							</div>
							<a href="index.html" class="btn-link">Go to Home</a>
						</div>
					</div>
				</div>
				<?php
					} 
					$conn->close(); 
				}

				else
					{ ?>
						<div class="content-inner">
						<div class="container">
						<div class="row" id="masonry">
						<div class="col-xl-6 col-lg-6 card-container">
						
						<div class="dz-card blog-grid style-1 m-b50 aos-item" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
							<div class="dz-media">
								<a href="index.html"><img src="images\blog\blog-grid\pic2.jpg" alt="blog image comes here"></a>
							</div>
							<div class="dz-info">
								<div class="dz-meta">
									
								</div>
								<h3 class="dz-title"><a href="index.html" id = "titleText">Connection problem</a></h3>
								<div class="dz-post-text text">
									<p id = "paraText">Couldn't connect to network</p>
								</div>
								<a href="index.html" class="btn-link">Go to Home</a>
							</div>
						</div>
					</div>	
				<?php	} ?>
			</div>
		</div>		
	</div>
</div>	

<!-- JAVASCRIPT FILES ========================================= -->
<script src="js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- BOOTSTRAP.MIN JS -->
<script src="vendor/rangeslider/rangeslider.js"></script><!-- RANGESLIDER -->
<script src="vendor/swiper/swiper-bundle.min.js"></script><!-- OWL-CAROUSEL -->
<script src="vendor/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
<script src="vendor/masonry/masonry-4.2.2.js"></script><!-- MASONRY -->
<script src="vendor/lightgallery/js/lightgallery-all.min.js"></script><!-- LIGHTGALLERY -->
<script src="js/dz.carousel.js"></script><!-- OWL-CAROUSEL -->
<script src="vendor/aos/aos.js"></script><!-- AOS -->
<script src="js/dz.ajax.js"></script><!-- AJAX -->
<script src="js/custom.js"></script><!-- CUSTOM JS -->
</body>
</html>
<?php
	}
	else{
		header("location:loginPage.php");
		exit();
	}
